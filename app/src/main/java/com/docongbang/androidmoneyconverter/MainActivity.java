package com.docongbang.androidmoneyconverter;

import android.content.Context;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    public static SensorManager SENSOR_MANAGER;
    public final static String LOG_TAG = "MC_DEBUG";

    private MCFieldModel inputFieldModel;
    private MCFieldView inputFieldView;
    private MCFieldController inputFieldController;

    private MCFieldModel outputFieldModel;
    private MCFieldView outputFieldView;
    private MCFieldController outputFieldController;

    private MCGlobalModel model;
    private MCGlobalController controller;
    private MCGlobalView view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SENSOR_MANAGER = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        model = new MCGlobalModel("DOLLAR", "EURO");
        view = (MCGlobalView) findViewById(R.id.mcGlobalView);
        controller = new MCGlobalController(view, model);

        inputFieldModel = model.getInputFieldModel();
        inputFieldView = (MCFieldView) findViewById(R.id.inputFieldView);
        inputFieldController = new MCFieldController(inputFieldView, inputFieldModel);

        outputFieldModel = model.getOutputFieldModel();
        outputFieldView = (MCFieldView) findViewById(R.id.outputFieldView);
        outputFieldController = new MCFieldController(outputFieldView, outputFieldModel);

        controller.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
