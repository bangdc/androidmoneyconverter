package com.docongbang.androidmoneyconverter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bangdc on 15/01/17.
 */

public class MCGlobalModel {
    private String currencyInputLabel;
    private String currencyOutputLabel;

    private List<MCGlobalView> views;

    private MCFieldModel inputFieldModel;
    private MCFieldModel outputFieldModel;

    public MCGlobalModel(String currencyInputLabel, String currencyOutputLabel) {
        this.currencyInputLabel = currencyInputLabel;
        this.currencyOutputLabel = currencyOutputLabel;

        views = new ArrayList<MCGlobalView>();

        inputFieldModel = new MCFieldModel(currencyInputLabel, "", true);
        outputFieldModel = new MCFieldModel(currencyOutputLabel, "", false);
    }

    public void addView(MCGlobalView view) {
        views.add(view);
    }

    public void notifyViews() {
        for(MCGlobalView view : views) {
            view.getController().updateView();
        }
    }

    public String getCurrencyInputLabel() {
        return currencyInputLabel;
    }

    public void setCurrencyInputLabel(String currencyInputLabel) {
        this.currencyInputLabel = currencyInputLabel;
    }

    public String getCurrencyOutputLabel() {
        return currencyOutputLabel;
    }

    public void setCurrencyOutputLabel(String currencyOutputLabel) {
        this.currencyOutputLabel = currencyOutputLabel;
    }

    public MCFieldModel getOutputFieldModel() {
        return outputFieldModel;
    }

    public void setOutputFieldModel(MCFieldModel outputFieldModel) {
        this.outputFieldModel = outputFieldModel;
    }

    public MCFieldModel getInputFieldModel() {
        return inputFieldModel;
    }

    public void setInputFieldModel(MCFieldModel inputFieldModel) {
        this.inputFieldModel = inputFieldModel;
    }
}
