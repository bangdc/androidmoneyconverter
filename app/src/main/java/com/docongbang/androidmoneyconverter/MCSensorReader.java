package com.docongbang.androidmoneyconverter;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

/**
 * Created by bangdc on 25/01/17.
 */

public class MCSensorReader extends Thread implements SensorEventListener {
    private Sensor accSensor;
    private MCSensorStock sensorStock;

    public MCSensorReader(MCSensorStock sensorStock) {
        super();
        Log.d(MainActivity.LOG_TAG, "Create instance of sensor reader.");
        this.sensorStock = sensorStock;
        this.accSensor = MainActivity.SENSOR_MANAGER.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    @Override
    public void run() {
        MainActivity.SENSOR_MANAGER.registerListener(this, accSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        sensorStock.setFirstAccValue(sensorEvent.values[0]);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
