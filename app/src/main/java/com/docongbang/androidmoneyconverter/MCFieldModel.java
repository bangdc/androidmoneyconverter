package com.docongbang.androidmoneyconverter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bangdc on 16/01/17.
 */

public class MCFieldModel {
    private String labelText;
    private String inputValue;
    private boolean enable;
    private List<MCFieldView> views;

    public MCFieldModel(String labelText, String inputValue, Boolean enable) {
        this.labelText = labelText;
        this.inputValue = inputValue;
        this.enable = enable;
        views = new ArrayList<MCFieldView>();
    }

    public String getLabelText() {
        return labelText;
    }

    public String getInputValue() {
        return inputValue;
    }

    public boolean isEnable() {
        return enable;
    }

    public void addView(MCFieldView view) {
        views.add(view);
    }

    public void setInputValue(String inputValue) {
        this.inputValue = inputValue;
    }

    public void setLabelText(String labelText) {
        this.labelText = labelText;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public void removeView(MCFieldView view) {
        views.remove(view);
    }

    public void notifyViews() {
        for(MCFieldView view : views) {
            view.getController().updateView();
        }
    }
}
