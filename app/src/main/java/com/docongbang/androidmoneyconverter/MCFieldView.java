package com.docongbang.androidmoneyconverter;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by bangdc on 15/01/17.
 */

public class MCFieldView extends RelativeLayout {
    private View root;
    private TextView currencyLabel;
    private EditText amountInput;

    private MCFieldModel model;
    private MCFieldController controller;

    public void init(Context context) {
        root = inflate(context, R.layout.mc_field, this);
        currencyLabel = (TextView) root.findViewById(R.id.currencyLabel);
        amountInput = (EditText) root.findViewById(R.id.amountInput);
    }

    public MCFieldView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public MCFieldView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public MCFieldView(Context context) {
        super(context);
        init(context);
    }

    public void setModel(MCFieldModel model) {
        this.model = model;
    }

    public void setController(MCFieldController controller) {
        this.controller = controller;
    }

    public TextView getCurrencyLabel() {
        return currencyLabel;
    }

    public EditText getAmountInput() {
        return amountInput;
    }

    public void update() {
        currencyLabel.setText(model.getLabelText());
        amountInput.setText(model.getInputValue());
        amountInput.setEnabled(model.isEnable());
    }

    public MCFieldController getController() {
        return controller;
    }

    public MCFieldModel getModel() {
        return model;
    }
}
