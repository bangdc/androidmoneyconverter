package com.docongbang.androidmoneyconverter;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Switch;

/**
 * Created by bangdc on 15/01/17.
 */

public class MCGlobalView extends RelativeLayout {
    private View root;
    private Button btnConvert;
    private Switch btnSwitch;

    private MCGlobalController controller;
    private MCGlobalModel model;

    public void init(Context context) {
        root = inflate(context, R.layout.mc_global, this);
        btnConvert = (Button) root.findViewById(R.id.btnConvert);
        btnSwitch = (Switch) root.findViewById(R.id.btnSwitch);
    }

    public MCGlobalView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public MCGlobalView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public MCGlobalView(Context context) {
        super(context);
        init(context);
    }

    public void setController(MCGlobalController controller) {
        this.controller = controller;
    }

    public MCGlobalController getController() {
        return controller;
    }

    public void setModel(MCGlobalModel model) {
        this.model = model;
    }

    public MCGlobalModel getModel() {
        return model;
    }

    public Button getBtnConvert() {
        return btnConvert;
    }

    public Switch getBtnSwitch() {
        return btnSwitch;
    }

    public void update() {
    }
}
