package com.docongbang.androidmoneyconverter;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.GestureDetector;

/**
 * Created by bangdc on 16/01/17.
 */

public class MCFieldController implements TextWatcher {
    private MCFieldView view;
    private MCFieldModel model;

    private GestureDetector gestureDetector;

    public MCFieldController(MCFieldView view, MCFieldModel model) {
        this.view = view;
        this.model = model;

        view.setModel(model);
        view.setController(this);

        model.addView(view);
        updateView();

        view.getAmountInput().addTextChangedListener(this);
    }

    public void setModel(MCFieldModel model) {
        this.model = model;
    }

    public void setView(MCFieldView view) {
        this.view = view;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (!view.getCurrencyLabel().getText().toString().equals(model.getLabelText())) {
            model.setLabelText(view.getCurrencyLabel().getText().toString());
        }
        if (!view.getAmountInput().getText().toString().equals(model.getInputValue())) {
            model.setInputValue(view.getAmountInput().getText().toString());
        }
    }

    public void updateView() {
        view.getCurrencyLabel().setText(model.getLabelText());
        view.getAmountInput().setText(model.getInputValue());
        view.getAmountInput().append("");
        view.getAmountInput().setEnabled(model.isEnable());
    }
}
