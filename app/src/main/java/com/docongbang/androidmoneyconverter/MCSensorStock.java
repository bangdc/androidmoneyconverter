package com.docongbang.androidmoneyconverter;

/**
 * Created by bangdc on 25/01/17.
 */

public class MCSensorStock {
    private float firstAccValue;

    public synchronized void setFirstAccValue(float firstAccValue) {
        this.firstAccValue = firstAccValue;
    }

    public synchronized float getFirstAccValue() {
        return firstAccValue;
    }
}
