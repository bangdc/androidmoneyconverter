package com.docongbang.androidmoneyconverter;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by bangdc on 15/01/17.
 */

public class MCGlobalController extends AsyncTask<Void, Float, Void> implements View.OnClickListener, View.OnTouchListener, GestureDetector.OnGestureListener {
    private static final float DOLLAR_TO_EUR_RATE = 0.94f;
    private static final float EUR_TO_DOLLAR_RATE = 1.07f;
    private MCGlobalView view;
    private MCGlobalModel model;

    private GestureDetector gestureDetector;

    private MCSensorStock sensorStock;
    private MCSensorReader sensorReader;

    public MCGlobalController(MCGlobalView view, final MCGlobalModel model) {
        this.view = view;
        this.model = model;

        view.setController(this);
        view.setModel(model);

        model.addView(view);

        updateView();

        view.getBtnConvert().setOnClickListener(this);
        view.getBtnSwitch().setOnClickListener(this);
        view.setOnTouchListener(this);
        gestureDetector = new GestureDetector(view.getContext(), this);

        this.sensorStock = new MCSensorStock();
        this.sensorReader = new MCSensorReader(sensorStock);
    }

    @Override
    protected Void doInBackground(Void... voids) {
        sensorReader.start();
        while (true) {
            publishProgress(sensorStock.getFirstAccValue());
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onProgressUpdate(Float... values) {
        if (Math.abs(values[0].floatValue()) > 6) {
            resetFields();
        }
    }

    private void resetFields() {
        model.getInputFieldModel().setInputValue("");
        model.getOutputFieldModel().setInputValue("");
        model.getInputFieldModel().notifyViews();
        model.getOutputFieldModel().notifyViews();
    }

    @Override
    public void onClick(View v) {
        if (v == view.getBtnConvert()) {
            Log.d(MainActivity.LOG_TAG, "The button was clicked");
            String currencyInput = model.getCurrencyInputLabel();
            String currencyOutput = model.getCurrencyOutputLabel();

            float rate = 0.0f;

            if (currencyInput == "DOLLAR" && currencyOutput == "EURO") {
                rate = DOLLAR_TO_EUR_RATE;
            } else if (currencyInput == "EURO" && currencyOutput == "DOLLAR") {
                rate = EUR_TO_DOLLAR_RATE;
            }

            Float amountInput = Float.valueOf(model.getInputFieldModel().getInputValue());
            Float amountOutput = amountInput * rate;

            model.getInputFieldModel().setInputValue(String.valueOf(amountInput));
            model.getOutputFieldModel().setInputValue(String.valueOf(amountOutput));
            model.getInputFieldModel().notifyViews();
            model.getOutputFieldModel().notifyViews();
        }

        if (v == view.getBtnSwitch()) {
            Log.d(MainActivity.LOG_TAG, "The switch was clicked");
            if (view.getBtnSwitch().isChecked()) {
                model.setCurrencyInputLabel("DOLLAR");
                model.setCurrencyOutputLabel("EURO");
            } else {
                model.setCurrencyInputLabel("EURO");
                model.setCurrencyOutputLabel("DOLLAR");
            }

            model.getInputFieldModel().setLabelText(model.getCurrencyInputLabel());
            model.getOutputFieldModel().setLabelText(model.getCurrencyOutputLabel());

            resetFields();
        }

        updateView();
    }

    public void setModel(MCGlobalModel model) {
        this.model = model;
    }

    public void setView(MCGlobalView view) {
        this.view = view;
    }

    public void updateView() {
        Log.d(MainActivity.LOG_TAG, "Global view was updated");
        if (model.getCurrencyInputLabel() == "DOLLAR" && model.getCurrencyOutputLabel() == "EURO") {
            view.getBtnSwitch().setChecked(true);
        }
        if (model.getCurrencyInputLabel() == "EURO" && model.getCurrencyOutputLabel() == "DOLLAR") {
            view.getBtnSwitch().setChecked(false);
        }
    }

    @Override
    public boolean onDown(MotionEvent motionEvent) {
        //Return true here, so that the TAP can not be considered as LONG PRESS
        return true;
    }

    @Override
    public void onShowPress(MotionEvent motionEvent) {
    }

    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        //Return true here, so that the TAP can not be considered as LONG PRESS
        return true;
    }

    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent motionEvent) {
        resetFields();
    }

    @Override
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        return false;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return gestureDetector.onTouchEvent(motionEvent);
    }
}
